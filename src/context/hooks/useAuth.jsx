import { useState } from "react";
import { api } from "../../service/api";
import { useNavigate } from "react-router-dom";


export function useAuth() {
    const [user, setUser] = useState('')
    const [token, setToken] = useState('')
    const [isLogged, setIsLogged] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    const handleLogin = (body, navigate) =>{
        setIsLoading(true)
        api.post('/login', body).then(res => {
            setUser(res.data.user)
            setToken(res.data.token)
            localStorage.setItem('user', JSON.stringify(res.data.user))
            localStorage.setItem('token', JSON.stringify(res.data.token))
            setIsLogged(true)
            navigate('/feed')
            api.defaults.headers.authorization = `bearer ${res.data.token}` 
        }).catch(err => console.log(err))
        setIsLoading(false)

        
    }

    const handleLogout = (navigate) => {
        setUser('')
        setToken('')
        localStorage.removeItem('user')
        localStorage.removeItem('token')
        setIsLogged(false)
        api.defaults.headers.authorization = '' 
        // navigate('/')
    }

    return {handleLogin, user, token, isLogged, handleLogout, isLoading}
}