import { createContext } from 'react'
import { useAuth } from './hooks/useAuth'


const GlobalContext = createContext()

function GlobalProvider( { children }) {
    const {handleLogin, user, token, isLogged, handleLogout, isLoading} = useAuth()
    
    return(
        <GlobalContext.Provider value={ {handleLogin, user, token, isLogged, handleLogout, isLoading} } >
            { children }
        </GlobalContext.Provider>
    )

}

export {GlobalContext, GlobalProvider}
