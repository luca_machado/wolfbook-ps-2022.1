import { useContext } from "react";
import { GlobalContext } from "../../context/GlobalContext";


export function Feed(){

    const context =  useContext(GlobalContext)
    
    return(
        <h1>Bem-vindo ao seu feed {context.user.name}</h1>

    )
}