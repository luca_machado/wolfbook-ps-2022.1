import {BrowserRouter as Router, Routes, Route , Outlet, Navigate} from 'react-router-dom'
import { Login } from '../pages/login/Login'
import { Error404 } from '../pages/error404/Error404'
import { AboutUs } from '../pages/aboutUs/AboutUs'
import { Feed } from '../pages/feed/Feed'
import { Header } from '../components/header/Header'
import { useContext } from 'react'
import { GlobalContext } from '../context/GlobalContext'

export function AppRoutes() {
    const context = useContext(GlobalContext)
    
    function CustomRoute() {
        const isLogged = context.isLogged
        console.log(isLogged)
        return isLogged ? <Outlet/> : <Navigate to="/"/>
    }

    function LoginRoute() {
        const isLogged = context.isLogged
        return isLogged ? <Navigate to="/feed"/> : <Outlet/>
    }
    
    return(
    <Router>
        <Header/>
        <Routes>
            <Route path={'/'} element={<LoginRoute/>} >
                <Route path={'/'} element={<Login/>}/>
            </Route>
            <Route path={'/quem-somos'} element={<AboutUs/>}/>
            
            <Route path={'/feed'} element={<CustomRoute/>} >
                <Route path={'/feed'} element={<Feed/>}/>
            </Route>

            <Route path='*' element={<Error404/>}/>
        </Routes>
    </Router>
    )
}