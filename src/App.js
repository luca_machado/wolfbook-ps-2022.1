import './App.css';
import { Header } from './components/header/Header';
import { Login } from './pages/login/Login';
import { GlobalProvider } from './context/GlobalContext';
import {AppRoutes} from './routes/Routes'

function App() {
  return (
  <GlobalProvider>
    <AppRoutes></AppRoutes>
  </GlobalProvider>
  
  );
}

export default App;
