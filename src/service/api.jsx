import axios from 'axios'

const api = axios.create({
    baseURL: 'https://wolfbook.herokuapp.com'
})

export { api }