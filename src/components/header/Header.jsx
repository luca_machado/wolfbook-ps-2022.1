import classes from './Header.module.css'
import { NavLink, Link } from 'react-router-dom'
import { useContext } from 'react'
import { GlobalContext } from '../../context/GlobalContext'

export function Header() {
    const context = useContext(GlobalContext) 
    return(
        <header className={classes.header}>
            <nav>
                <div className={classes.logo}>
                <h1>Wolf-Book</h1>
                </div>

                <ul className={classes.links}>
                    {!context.isLogged && <li> <NavLink to='/'> Login </NavLink> </li>}
                    {context.isLogged && <li> <NavLink to='feed' > Feed </NavLink> </li>}
                    <li> <NavLink activeClassName={classes.active} to='quem-somos'> Quem somos </NavLink> </li>
                    {context.isLogged && <li> <Link onClick={context.handleLogout} to='/'> Logout </Link> </li>}
                </ul>
            </nav>
        </header>
    )
}