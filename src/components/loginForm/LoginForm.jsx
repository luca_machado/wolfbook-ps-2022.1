import classes from "./LoginForm.module.css";
import { GlobalContext } from "../../context/GlobalContext";
import { useContext, useState, useRef} from "react";
import { useNavigate } from "react-router-dom";
import reactSpinner from 'react'


export function LoginForm() {
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const emailRef = useRef()
  const passwordRef = useRef()

  const navigate = useNavigate()
  const context = useContext(GlobalContext)

  let body = {
     "user":{
    email,
    password
  }
}

  function handleForm(e) {
    e.preventDefault()
    console.log(body)
    context.handleLogin(body, navigate)
  }

  return (
    <body className={classes.body}>
      <div className={classes.card}>
        <h1>Faça o seu Login</h1>
        
        {!context.isLoading ? <>
           <form action="" className={classes.form} onSubmit={handleForm}>
          <div className={classes.input}>
            <label htmlFor="login">Login:</label>
            <input type="text" name="login" ref={emailRef} />
          </div>
          <div className={classes.input}>
            <label htmlFor="password">Senha: </label>
            <input type="password" name="password" ref={passwordRef} />
          </div>
          <button type={"submmit"} className={classes.button}>
            Logar
          </button>
        </form>
          </> : <p>carregando...</p>}
      </div>
    </body>
  );
}
